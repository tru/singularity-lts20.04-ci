# building an Ubuntu LTS 20.04 singularity image for CI

Tru <tru@pasteur.fr>

(toy) singularity image produced available at `registry-gitlab.pasteur.fr/tru/singularity-lts20.04-ci:latest` (if tagged from the main branch)
